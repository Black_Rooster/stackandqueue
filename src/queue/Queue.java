/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queue;

/**
 *
 * @author senzo
 */
public class Queue {

    private int[] queue;
    private final int HEAD = 0;
    private int tail = 0;

    public Queue(int size) {
        queue = new int[size];
    }

    public void Enqueue(int x) {
        if (IsFull()) {
            System.err.println("Queue is full");
            return;
        }

        queue[tail] = x;
        tail++;
    }

    public int Dequeue() throws Exception {

        if (IsEmpty()) {
            throw new Exception("Queue is empty");
        }

        int value = queue[HEAD];
        for (int i = 0; i < (queue.length - 1); i++) {
            queue[i] = queue[i + 1];
        }
        queue[queue.length - 1] = 0;

        return value;
    }

    public boolean IsFull() {
        if (tail == queue.length) {
            return true;
        }

        return false;
    }
    
    public boolean IsEmpty(){
        if(tail == 0){
            return true;
        }
        
        return false;
    }
    
    public void Display(){
        for (int i = 0; i < queue.length; i++) {
            System.out.print(queue[i] + " ");
        }
    }
}

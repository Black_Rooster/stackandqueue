/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queue;

/**
 *
 * @author senzo
 */
public class TestQueue {

    public static void main(String[] args) {
        Queue numbers = new Queue(5);

        numbers.Enqueue(6);
        numbers.Enqueue(12);
        numbers.Enqueue(18);
        numbers.Enqueue(24);
        numbers.Enqueue(30);
        numbers.Enqueue(36);
        numbers.Enqueue(42);

        numbers.Display();
        System.out.println("");
        System.out.println(numbers.IsFull());
        
    }
}

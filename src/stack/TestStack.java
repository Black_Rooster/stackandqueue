/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stack;

/**
 *
 * @author senzo
 */
public class TestStack {
    public static void main(String[] rags)
    {
        Stack numbers = new Stack();
        
        numbers.Push(15);
        numbers.Display();
        numbers.Push(2);
        numbers.Display();
        numbers.Push(236);
        numbers.Display();
        numbers.Push(100);
        numbers.Display();
        numbers.Push(7);
        numbers.Display();
    
        
        System.out.println("Popped Value " + numbers.Pop());
        numbers.Display();
        System.out.println("Popped Value " + numbers.Pop());
        numbers.Display();
        
        System.out.println("Top Value: " + numbers.Peek());
    }
}

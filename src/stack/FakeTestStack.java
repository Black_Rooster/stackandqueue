/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stack;

/**
 *
 * @author senzo
 */
public class FakeTestStack {

    public static void main(String[] args) {
        try {
            FakeStack numbers = new FakeStack(5);

            numbers.Push(56);
            numbers.Push(78);
            numbers.Push(2000);
            numbers.Push(90);
            numbers.Push(100);

            System.out.println(numbers.Pop());
            System.out.println(numbers.Pop());
            System.out.println(numbers.Pop());
            System.out.println(numbers.Pop());
            System.out.println(numbers.Pop());
        } catch (Exception ex) {
            System.err.print(ex.getMessage());
        }
    }
}

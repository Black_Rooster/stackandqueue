/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stack;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author senzo
 */
public class Stack {
    
    private List<Integer> stack = new ArrayList<>();
    private int top;
    
    public void Push(int x){
        stack.add(x);
    }
    
    public int Pop(){
        top = stack.size() -1;
        int popValue = stack.get(top);
        stack.remove(top);
        
        return popValue;
    }
    
    public int Peek(){
         top = stack.size() -1;
        int peekValue = stack.get(top);
        
        return peekValue;
    }
    
    public void Display()
    {
        System.out.println("Our Stack" + stack);
    }
}

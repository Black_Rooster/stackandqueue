/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stack;

/**
 *
 * @author senzo
 */
public class FakeStack {

    private int[] stack;
    private int top = -1;

    public FakeStack(int size) {
        stack = new int[size];
    }

    public void Push(int x) {
        boolean isStackFull = IsFull();
        if(isStackFull == true){
            System.err.println("Stack is full");
            return;
        }
        
        top++;
        stack[top] = x;
    }

    public int Pop() throws Exception{
        if(IsEmpty() == true){
            throw new Exception("Stack is empty!!!");
        }
        
        int value = stack[top];
        stack[top] = 0;
        top--;
        
        return value;
    }
    
    public boolean IsFull(){
        if(top >= stack.length){
            return true;
        }
        
        return false;
    }
    
    public boolean IsEmpty() {
        if (top == -1) {
            return true;
        }

        return false;
    }
    
    public void Display(){
        for (int i = (stack.length-1); i > 0; i++) {
            System.out.print(stack[i] + " ");
        }
    }
}
